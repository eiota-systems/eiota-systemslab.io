class AnimatedLine {
  constructor(points, context, color, stroke, steps, callback){
    this.ctx = context; 
    this.color = color;
    this.stroke = stroke;
    this.callback = callback;
    this.points = this.waypoints(points, steps);
  }

  waypoints(vertices, steps){
  	var waypoints = [];
    for (var i = 1; i < vertices.length; i++) {
    	var pt0 = vertices[i - 1];
      var pt1 = vertices[i];
      var dx = pt1.x - pt0.x;
      var dy = pt1.y - pt0.y;
      for (var j = 0; j < steps; j++) {
      	var x = pt0.x + dx * j / steps;
        var y = pt0.y + dy * j / steps;
        waypoints.push({x: x, y: y});
      }
    }
    waypoints.push({x: vertices[vertices.length-1].x , y: vertices[vertices.length-1].y});
    return (waypoints);
  }

	animate(){
		let t = 1;
		const anim = (ts) => {
			if (t < this.points.length - 1) {
      	requestAnimationFrame(anim);
      }else{
				this.callback();
      }
      this.draw_line(this.points[t - 1].x, this.points[t - 1].y, this.points[t].x, this.points[t].y)
			t++;
		}
    requestAnimationFrame(anim);
  }

  draw_line(x1, y1, x2, y2){
    this.ctx.beginPath();
		this.ctx.lineWidth = this.stroke;
    this.ctx.strokeStyle = this.color;
    this.ctx.moveTo(x1, y1);
    this.ctx.lineTo(x2, y2);
    this.ctx.stroke();
  }
}
