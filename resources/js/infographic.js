const offset = (x, y, width, height, points) => {
  let new_points = [];
  for(var p = 0; p < points.length; p++){
    new_points.push({x: points[p].x + (x*width), y: points[p].y + (y*height)})
  }
  return new_points;
};

let canvas = document.getElementById('infographic');

let c = canvas.getContext('2d');

let loaded = 0
let icons = [];
let num_icons = 24;

const check_loaded = (e) => {
  loaded++;
  if(loaded >= num_icons) animate();
}

for(var i = 1; i <= num_icons; i++){
  let bg = document.createElement('div');
  bg.classList.add('icon');
  let im = new Image();
  im.addEventListener('load', check_loaded)
  let image = i.toString().padStart(3, '0');
  let src = './resources/images/icons/white-icon'+image+'.svg';
  im.src = src;
  bg.appendChild(im);
  icons.push(bg);
}

let hero_image = document.getElementById('hero-image');
let g_width = 375;
let g_height = 400;
let window_width = canvas.offsetWidth;
let window_height = canvas.offsetHeight;
let points_left = [{x: 0, y: 0}, {x: 0, y: 100}, {x: 75, y: 100}, {x: 0, y: 0}];
let points_right = [{x: 0, y: 0}, {x: 0, y: 100}, {x: -75, y: 100}, {x: 0, y: 0}];
let points_left_down = [{x: 0, y: 0}, {x: 0, y: 100}, {x: 75, y: 0}, {x: 0, y: 0}];
let points_right_down = [{x: 0, y: 0}, {x: 0, y: 100}, {x: -75, y: 0}, {x: 0, y: 0}];
let width = 75;
let height = 100;
let steps = 7;
let stroke = 2;
let yellow = '#ffe000';
let pink = '#e286eb';
let aqua = '#04a2b2';
let orange = '#ff6700';
let mult_x = Math.min((window_width / g_width), 1);
let mult_y = Math.min((window_height / g_height), 1);


const place_icon = (x, y, id) => {
  let w = 75 * mult_x;
  let h = 100 * mult_y;
  icons[id].style.right = ((w * x) - 30) + "px";
  icons[id].style.top = ((y * h) - 30) + "px";
  hero_image.appendChild(icons[id]);
  setTimeout(() => {
    icons[id].classList.add('animate');
  }, 200);
}

let cb = () => console.log("done");
let i1 = new AnimatedLine(offset(3, 0, width, height, points_left), c, pink, stroke, steps, () => { 
  i4.animate();
  place_icon(2, 0, 20);
});

let i2 = new AnimatedLine(offset(3, 0, width, height, points_right), c, yellow, stroke, steps, () => i3.animate());
let i3 = new AnimatedLine(offset(3, 1, width, height, points_left_down), c, aqua, stroke, steps, () => {i6.animate(); place_icon(1, 2, 1); place_icon(3, 1, 14);});
let i4 = new AnimatedLine(offset(3, 1, width, height, points_right_down), c, orange, stroke, steps, () => i5.animate());
let i5 = new AnimatedLine(offset(4, 1, width, height, points_right), c, pink, stroke, steps, () => i8.animate());
let i6 = new AnimatedLine(offset(4, 1, width, height, points_left), c, yellow, stroke, steps, () => i7.animate());
let i7 = new AnimatedLine(offset(4, 2, width, height, points_right_down), c, aqua, stroke, steps, () => {
  i10.animate(); 
  i11.animate();
});
let i8 = new AnimatedLine(offset(4, 2, width, height, points_left_down), c, orange, stroke, steps, () => {i9.animate()});
let i9 = new AnimatedLine(offset(3, 2, width, height, points_left), c, pink, stroke, steps, () => {});
let i10 = new AnimatedLine(offset(3, 2, width, height, points_right), c, yellow, stroke, steps, () => {});
let i11 = new AnimatedLine(offset(5, 2, width, height, points_right), c, aqua, stroke, steps, () => {
  i12.animate(); 
  i13.animate();
});

let i12 = new AnimatedLine(offset(5, 3, width, height, points_right), c, orange, stroke, steps, () => {});
let i13 = new AnimatedLine(offset(4, 3, width, height, points_left_down), c, yellow, stroke, steps, () => {
  i14.animate();
  place_icon(2, 2, 23);
});

let i14 = new AnimatedLine(offset(3, 3, width, height, points_left), c, pink, stroke, steps, () => {i16.animate(); place_icon(2, 3, 3);});
//let i15 = new AnimatedLine(offset(4, 3, width, height, points_right_down), c, aqua, stroke, steps, () => {i16.animate();});

let i16 = new AnimatedLine(offset(3, 3, width, height, points_right), c, orange, stroke, steps, () => {i19.animate();});
//let i17 = new AnimatedLine(offset(3, 3, width, height, points_left_down), c, yellow, stroke, steps, () => {});

let i18 = new AnimatedLine(offset(1, 3, width, height, points_left), c, pink, stroke, steps, () => {i20.animate(); place_icon(3, 4, 19);});
let i19 = new AnimatedLine(offset(2, 3, width, height, points_right_down), c, aqua, stroke, steps, () => {place_icon(1, 4, 13);i18.animate()});

let i20 = new AnimatedLine(offset(1, 3, width, height, points_right), c, orange, stroke, steps, () => {place_icon(5, 4, 7);});

const animate = () =>{
  i1.animate();
  i2.animate();
}
